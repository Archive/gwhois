# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gwhois 0.0.1\n"
"POT-Creation-Date: 2001-12-26 11:30+0100\n"
"PO-Revision-Date: 2001-12-26 19:18GMT+0200\n"
"Last-Translator: Vasif İsmayıloğlu MD <azerb_linux@hotmail.com>\n"
"Language-Team: Azerbaijani Turkic <linuxaz@azerimail.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: src/main.c:49
msgid "Bjorn Lindgren <bjorn@500mhz.net>"
msgstr "Bjorn Lindgren <bjorn@500mhz.net>"

#: src/main.c:56
msgid "(C) 2000, 2001 Bjorn Lindgren"
msgstr "(C) 2000, 2001 Bjorn Lindgren"

#: src/main.c:57
msgid "A whois client for the GNOME desktop enviroment."
msgstr "GNOME masa üstü mühiti üçün whois alıcısı."

#: src/main.c:101
msgid "_New Window"
msgstr "_Yeni Pəncərə"

#: src/main.c:102
msgid "Open a new application window"
msgstr "Yeni təminat pəncərəsi aç"

#: src/main.c:103
msgid "_Clear History"
msgstr "_Keçmişi Təmizlə"

#: src/main.c:104
msgid "Clear object history"
msgstr "Cismə keçmişini təmizlə"

#: src/main.c:296
msgid "Specify the geometry of the main window: ex. WIDTHxHEIGHT+X+Y"
msgstr "Əsas pəncərənin kordinatlarını bildir: məs. ENxHÜNDÜRLÜK+X+Y"

#: src/main.c:297
msgid "GEOMETRY"
msgstr "GEOMETRY"

