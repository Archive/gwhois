/* tools/xwhois2gwhois.c: A tool for converting configuration files
 *                        from old xwhois format to gwhois XML format.
 *
 * Gwhois - The GNOME whois client.
 *
 * Copyright (C) 1998, 1999, 2000, 2001 Bjorn Lindgren <bjorn@500mhz.net>
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 1024

static const char rcsid[] = "$Id$";

static void
convert_server(const char *file)
{
	FILE *fp;
	char *ptr, buf[BUFSIZE], hostname[BUFSIZE];

	printf("<?xml version=\"1.0\"?>\n<!-- Last modified: $Id$ -->\n\n<servers>\n");
	fp = fopen(file, "r");
	while (fgets(buf, BUFSIZE-1, fp))
	{
		if (!(buf[0] == '\n' || buf[0] == '\r' || buf[0] == '#'))
		{
			printf("  <server>\n");
			sscanf(buf, "%s#", hostname);
			printf("    <hostname>%s</hostname>\n", hostname);
			printf("    <protocol>IPv4</protocol>\n");
			printf("    <ip></ip>\n");

			ptr = strdup(buf);
			ptr = strtok(ptr, "#");
			ptr = strtok(NULL, "#");
			++ptr;
			ptr[strlen(ptr) - 1] = '\0';
			printf("    <comment>%s</comment>\n", ptr);
			printf("  </server>\n");
			ptr = NULL;
		}
	}
	fclose(fp);
	printf("</servers>\n");
}

static void
convert_history(const char *file)
{
	printf("History conversion is not implemented yet.");
	return;
}

int
main(int argc, char **argv)
{
	if (argc < 3)
	{
		printf("Usage: %s [-s|-h] <config file>\n", argv[0]);
		exit(0);
	}

	if (!strcmp(argv[1], "-s"))
		convert_server(argv[2]);
	else if (!strcmp(argv[1], "-h"))
		convert_history(argv[2]);
	else
	{
		printf("%s: invalid option: %s\n", argv[0], argv[1]);
		printf("Usage: %s [-s|-h] <config file>\n", argv[0]);
		exit(1);
	}

	return 0;
}
