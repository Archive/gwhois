#!/bin/sh
# $Id$

if [ ! -d $HOME/.gwhois ]
then
  mkdir $HOME/.gwhois
fi

if [ -f $HOME/.xwhois/servers ]
then
  ./xwhois2gwhois -s $HOME/.xwhois/servers >$HOME/.gwhois/servers.xml
fi
