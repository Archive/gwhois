/* src/error.c - Warper functions for GNOME error dialogs.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 1998, 1999, 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>

#include "error.h"
#include "extern.h"

void
gwhois_error_dialog(char *errmsg)
{
	GtkWidget *dialog;

	dialog = gnome_message_box_new(errmsg, GNOME_MESSAGE_BOX_ERROR,
		GNOME_STOCK_BUTTON_OK, NULL);

	gnome_dialog_run_and_close(GNOME_DIALOG (dialog));
}
