/* src/toolbar.c - Implementation of the toolbar.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>

#include "toolbar.h"
#include "main.h"
#include "history.h"
#include "network.h"
#include "extern.h"

void
gwhois_toolbar(GtkWidget *main_box, GtkWidget *toplevel, GtkCombo *history_combo, GtkWidget *text_field_list)
{
	GtkWidget *box, *label, *button;
	GwhoisPackedWidgets *widgets;

	widgets = malloc(sizeof(GwhoisPackedWidgets));

	box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX (main_box), box, FALSE, FALSE, 5);

	label = gtk_label_new("Object");
	gtk_box_pack_start(GTK_BOX (box), label, FALSE, TRUE, 5);

	gtk_signal_connect(GTK_OBJECT(history_combo->list), "selection_changed",
		GTK_SIGNAL_FUNC(gwhois_history_selection), history_combo);
	if (history_list)
		gtk_combo_set_popdown_strings(history_combo, history_list);
	gtk_entry_set_text(GTK_ENTRY (history_combo->entry), "");
	gtk_box_pack_start(GTK_BOX (box), GTK_WIDGET (history_combo),
		TRUE, TRUE, 0);

	widgets->combo = history_combo;
	widgets->list = text_field_list;

	button = gtk_button_new_with_label(" Submit ");
	gtk_box_pack_start(GTK_BOX (box), button, FALSE, TRUE, 5);
	gtk_signal_connect(GTK_OBJECT (button), "clicked",
		GTK_SIGNAL_FUNC (gwhois_network), (gpointer) widgets);

	button = gtk_button_new_with_label(" < ");
	gtk_box_pack_start(GTK_BOX (box), button, FALSE, TRUE, 0);

	button = gtk_button_new_with_label(" > ");
	gtk_box_pack_start(GTK_BOX (box), button, FALSE, TRUE, 5);

	button = gtk_button_new_with_label(" ? ");
	gtk_box_pack_start(GTK_BOX (box), button, FALSE, TRUE, 0);

	button = gtk_button_new_with_label(" Scan ");
	gtk_box_pack_start(GTK_BOX (box), button, FALSE, TRUE, 5);

	return;
}
