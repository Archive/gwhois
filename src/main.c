/* src/main.c - Implementation of the main window functions.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>
#include <sys/stat.h>

#include "server.h"
#include "main.h"
#include "extern.h"
#include "history.h"
#include "toolbar.h"
#include "field.h"
#include "error.h"

static gchar *geometry = NULL;

static void
gwhois_about_cb(GtkWidget *widget, void *data)
{
	GtkWidget *about;
	const gchar *authors[] = {
		N_("Bjorn Lindgren <bjorn@500mhz.net>"),
		NULL
	};

#ifdef HAS_NLS
	authors[0] = _(authors[0]);
#endif
	about = gnome_about_new("Gwhois", VERSION, _("(C) 2000, 2001 Bjorn Lindgren"),
		authors, _("A whois client for the GNOME desktop enviroment."), NULL);
	gtk_widget_show(about);
	return;
}

static void
gwhois_close(GtkWidget *widget, gpointer data)
{
	GnomeWhoisApp *app = data;
	int counter = 0;

	while (app_index[counter].app) {
		if (app_index[counter].app == app) {
			app_index[counter].state = 0;
			break;
		}
		++counter;
	}

	gtk_widget_destroy(GTK_WIDGET(app->history_combo));
	gtk_widget_destroy(app->text_field_list);
	gtk_widget_destroy(app->gwhois);
	gtk_widget_destroy(app->statusbar);
	gtk_widget_destroy(app->app);
	free(app);

//	app_index[counter].app = NULL;

	if (ac == 1)
		gwhois_exit(NULL, NULL);
	--ac;
}

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_MENU_ABOUT_ITEM (gwhois_about_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {
	GNOMEUIINFO_MENU_PREFERENCES_ITEM(gwhois_preferences_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo file_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM (N_("_New Window"),
		N_("Open a new application window"), gwhois_app_new, NULL),
	GNOMEUIINFO_ITEM_STOCK (N_("_Clear History"),
		N_("Clear object history"), gwhois_history_delete, GNOME_STOCK_PIXMAP_TRASH),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CLOSE_ITEM (gwhois_close, NULL),
	GNOMEUIINFO_MENU_EXIT_ITEM(gwhois_exit, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),
	GNOMEUIINFO_MENU_SETTINGS_TREE(settings_menu),
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
	GNOMEUIINFO_END
};


void
gwhois_preferences_cb(void)
{
	printf("prefs callback..\n");
}

static void
gwhois_delete_event_cb(GtkWidget *widget, GdkEventAny *event, gpointer data)
{
	gwhois_close(NULL, data);
}


void
gwhois_exit(GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
}

static GtkWidget *
gwhois_init(GtkWidget *toplevel, GnomeWhoisApp *app)
{
	struct stat file_s;
	GtkWidget *main_box;
	static unsigned int gwhois_init_flag;
	gchar *file;

	app->history_combo = GTK_COMBO(gtk_combo_new());
	app->text_field_list = gtk_list_new();

	if (gwhois_init_flag == 0)
	{
		file = malloc(strlen(getenv("HOME")) + strlen("/.gwhois") + 1);
		sprintf(file, "%s/.gwhois", getenv("HOME"));
		if (stat(file, &file_s)) {
			if (mkdir(file, 0755)) {
				perror(file);
				exit(1);
			}
		}
		free(file);

		slist = NULL;
		hdoc = NULL;

		if ((file = gwhois_server_get_path()) == NULL)
		{
			gwhois_error_dialog("Failed to find whois server file.");
			exit(1);
		}
		else
			slist = gwhois_server_read(file);

		file = gwhois_history_get_path();

		if (stat(file, &file_s) == 0)
			hdoc = xmlParseFile(file);

		gwhois_init_flag = 1;
	}

	gwhois_history_insert(hdoc);
	main_box = gtk_vbox_new(FALSE, 0);
	gwhois_toolbar(main_box, toplevel, app->history_combo, app->text_field_list);
	gwhois_field(main_box, slist, app->history_combo, app->text_field_list);
	/* gwhois_statusbar(main_box); */
	/* gwhois_statusbar(TRUE); */

	return main_box;
}

/*
static gint
gwhois_idle(gpointer data)
{
	GnomeWhoisApp *app;
	static gfloat proc;
	static guint state;
	int i = 0;

	if (proc <= 0.0)
	{
		proc = 0.0;
		state = 0;
	}
	if (proc >= 1.0)
	{
		proc = 1.0;
		state = 1;
	}

	while (i < ac)
	{
		if (app_index[i].state == 1)
		{
			app = app_index[i++].app;
			gnome_appbar_set_progress(GNOME_APPBAR (app->statusbar), proc);
		}
	}

	if (state == 0)
		proc += 0.0001;
	else                   
		proc -= 0.0001;

	return 1;
}
*/

void
gwhois_app_new(void)
{
	GnomeWhoisApp *app;

	app_index = realloc(app_index, (ac + 1) * sizeof(struct _app_index));

	app = malloc(sizeof(GnomeWhoisApp));

	app_index[ac].app = app;
	app_index[ac].state = 1;

	app->app = gnome_app_new("gwhois", "Gwhois "VERSION"");

	app->statusbar = gnome_appbar_new(TRUE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar(GNOME_APP (app->app), app->statusbar);

	gnome_app_create_menus_with_data(GNOME_APP(app->app), main_menu, app);
	gnome_app_install_menu_hints(GNOME_APP(app->app), main_menu);

	app->gwhois = gwhois_init(app->app, app);
	gnome_app_set_contents(GNOME_APP (app->app), app->gwhois);

	gtk_signal_connect(GTK_OBJECT (app->app), "delete_event",
		GTK_SIGNAL_FUNC (gwhois_delete_event_cb), app);

	gtk_widget_show_all(app->app);

	++ac;
	++max_ac;
	app_index = realloc(app_index, (ac + 1) * sizeof(struct _app_index));
	app_index[ac].app = NULL;
	app_index[ac].state = 0;
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
		gint is_shutdown, GnomeInteractStyle interact_style,
		gint is_fast, gpointer client_data)
{
	gchar **argv;
	guint argc;

	/* allocate 0-filled, so it will be NULL-terminated */
	argv = g_malloc0(sizeof(gchar *) * 4);
	argc = 1;

	argv[0] = client_data;

	gnome_client_set_clone_command(client, argc, argv);
	gnome_client_set_restart_command(client, argc, argv);

	return TRUE;
}

static void
session_die(GnomeClient* client, gpointer client_data)
{
  gtk_main_quit();
}

int
main(int argc, char **argv)
{
	GnomeClient *client;

	static const struct poptOption options[] = {
		{"geometry", 'g', POPT_ARG_STRING, &geometry, 0, 
		N_("Specify the geometry of the main window: ex. WIDTHxHEIGHT+X+Y"), 
		N_("GEOMETRY")},

		{NULL, '\0', 0, NULL, 0, NULL, NULL}
	};

	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);

	ac = 0;
	gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, options, 0, NULL);

	/* session managment */
	client = gnome_master_client();
	gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		GTK_SIGNAL_FUNC (save_session), argv[0]);
	gtk_signal_connect (GTK_OBJECT (client), "die",
		GTK_SIGNAL_FUNC (session_die), NULL);

	gwhois_app_new();

	/* gtk_idle_add(gwhois_idle, NULL); */
	gtk_main();
	return 0;
}
