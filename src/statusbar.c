/* src/statusbar.c
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 1998, 1999, 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>
#include <pthread.h>

#include "statusbar.h"

#include "extern.h"

void
gwhois_statusbar(int bol)
{
	pthread_t tid;

	if (bol == TRUE)
		pthread_create(&tid, NULL, gwhois_statusbar_t, NULL);

//	pthread_join(tid, NULL);
}

void *
gwhois_statusbar_t(void *arg)
{
	gfloat test = 0.0;
	while (1)
	{
		gnome_appbar_set_progress(GNOME_APPBAR (statusbar), test);
		sleep(5);
		test += 0.01;
		if (test > 0.9) test = 0.0;
	}

	/*
	while (proc < 1.0)
	{
		printf("%f\n", proc);
		gnome_appbar_set_progress(GNOME_APPBAR (statusbar), );
		gnome_appbar_refresh (GNOME_APPBAR (statusbar));
		proc += 0.01;
	}
	*/
	return NULL;
}
