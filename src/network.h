/* src/network.h
 *
 * $Id$
 */

#ifndef __GWHOIS_NETWORK_H__
#define __GWHOIS_NETWORK_H__

/* prototypes */

extern void gwhois_network (GtkWidget *, gpointer);

extern char * gwhois_network_query (char *, char *);

#endif /* __GWHOIS_NETWORK_H__ */
