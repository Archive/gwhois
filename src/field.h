/* src/field.h
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 1998, 1999, 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * $Id$
 */

#ifndef __GWHOIS_FIELD_H__
#define __GWHOIS_FIELD_H__

//GtkWidget *text_field, *text_field_list, *server_list;

GtkWidget *text_field, *server_list;

extern void gwhois_field_set_text (char *, GtkWidget *);

extern void gwhois_field (GtkWidget *, GnomeWhoisListServer *, GtkCombo *, GtkWidget *);

#endif /* __GWHOIS_FIELD_H__ */
