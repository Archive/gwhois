/* src/field.c
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>

#include "server.h"
#include "field.h"
#include "toolbar.h"
#include "history.h"
#include "network.h"
#include "extern.h"

const gchar *list_item_data_key = "list_item_data";

static void
gwhois_field_text_selection(GtkWidget *text_list, gpointer data)
{
	GtkCombo *history_combo = GTK_COMBO(data);
	GList *dlist;

	dlist = GTK_LIST (text_list)->selection;

	if (dlist) {
		GtkObject *list_item;
		gchar *item_data_string;
		char *str, *ptr, *node = NULL;

		list_item = GTK_OBJECT (dlist->data);
		item_data_string = gtk_object_get_data(list_item, list_item_data_key);
		str = strdup(item_data_string);
		ptr = str;

		/* locate node entry */
		while (*str != '\0') {
			if (*str == '(') {
				node = str;
				*node = '!';
			}

			if (*str == ')')
				*str = '\0';

			str++;
		}

		if (node) {
			/* dont query nodes that are FAX numbers */
			if (!strcmp(node, "!FAX"))
				return;

			gtk_entry_set_text(GTK_ENTRY (history_combo->entry), node);
			gwhois_network(NULL, NULL);
		}

		str = ptr;
		free(str);
	}
}

static void
gwhois_field_selection(GtkWidget *server_list, gpointer data)
{
	GList *dlist;

	dlist = GTK_LIST (server_list)->selection;

	if (dlist) {
		GtkObject *list_item;
		gchar *item_data_string;

		list_item = GTK_OBJECT (dlist->data);
		item_data_string = gtk_object_get_data(list_item, list_item_data_key);

		if (server)
			free(server);
		server = strdup(item_data_string);
	}
}

void
gwhois_field_set_text(char *new_text, GtkWidget *text_field_list)
{
	GtkWidget *list_item;
	GList *old_list;
	static char *text = NULL;
	char *ptr, *EOL;

	old_list = gtk_container_children(GTK_CONTAINER(text_field_list));
	while (old_list) {
		gtk_container_remove(GTK_CONTAINER(text_field_list), old_list->data);
		old_list = old_list->next;
	}

	text = strdup(new_text);

	EOL = text;
	ptr = EOL;
	while (*EOL != '\0') {
		if (*EOL == '\n') {
			*EOL = '\0';
			list_item = gtk_list_item_new_with_label(ptr);
			gtk_object_set_data(GTK_OBJECT (list_item), list_item_data_key, ptr);
			gtk_container_add(GTK_CONTAINER (text_field_list), list_item);
			gtk_widget_show(list_item);
			ptr = EOL + 1;
		}
		EOL++;
	}
}

void
gwhois_field(GtkWidget *main_box, GnomeWhoisListServer *slist, GtkCombo *history_combo, GtkWidget *text_field_list)
{
	GnomeWhoisListServer *ptr = slist;
	GtkWidget *box, *scrolled_window, *list_item;
	GtkTooltips *tooltips;

	box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX (main_box), box, TRUE, TRUE, 5);

	text_field = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (text_field),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_box_pack_start(GTK_BOX (box), text_field, TRUE, TRUE, 0);
	gtk_widget_set_usize(text_field, 350, 200);

//	text_field_list = gtk_list_new();
	gtk_signal_connect(GTK_OBJECT (text_field_list), "selection_changed",
		GTK_SIGNAL_FUNC (gwhois_field_text_selection), history_combo);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW (text_field),
		text_field_list);

	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_box_pack_start(GTK_BOX (box), scrolled_window, FALSE, TRUE, 0);
	gtk_widget_set_usize(scrolled_window, 190, 0);

	server_list = gtk_list_new();
	gtk_signal_connect(GTK_OBJECT (server_list), "selection_changed",
		GTK_SIGNAL_FUNC (gwhois_field_selection), NULL);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW
		(scrolled_window), server_list);

	tooltips = gtk_tooltips_new();

	while (1) {
		list_item = gtk_list_item_new_with_label(slist->hostname);
		gtk_container_add(GTK_CONTAINER (server_list), list_item);

		if (slist->comment)
			gtk_tooltips_set_tip(tooltips, list_item,
				slist->comment, NULL);

		gtk_object_set_data(GTK_OBJECT (list_item), list_item_data_key, slist->hostname);

		if (slist->next == NULL)
			break;
		slist = slist->next;
	}

	slist = ptr;
	gtk_list_select_item(GTK_LIST(server_list), 0);
}
