#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>

#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include "extern.h"
#include "toolbar.h"
#include "network.h"
#include "history.h"
#include "server.h"
#include "field.h"

#define TIMEOUT 10

static const char rcsid[] = "$Id$";

int sockfd = -1;

static void
gwhois_network_sigfunc(int signo)
{
	/* FIXME: this is ugly, but otherwise SA_RESTART will restart our connect syscall */
	close(sockfd);
	return;
}

void
gwhois_network(GtkWidget *widget, gpointer data)
{
	char *object = NULL, *text;
	GwhoisPackedWidgets *widgets = (GwhoisPackedWidgets *) data;
	GtkCombo *history_combo;
	GtkWidget *text_field_list;
	
	history_combo = widgets->combo;
	text_field_list = widgets->list;

	object = malloc(strlen(gtk_entry_get_text(GTK_ENTRY(history_combo->entry))) + 2);
	sprintf(object, "%s", gtk_entry_get_text(GTK_ENTRY(history_combo->entry)));

	if (strlen(object) < 1)
		return;

	text = gwhois_network_query(object, server);

	if (text)
		if (!gwhois_history_check_object(object))
			gwhois_history_write(object, server, history_combo);
	free(object);

	if (text)
		gwhois_field_set_text(text, text_field_list);
	free(text);
}

char *
gwhois_network_query(char *object, char *server)
{
	struct addrinfo hints, *res, *aip;
	struct timeval tv;
	char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV], buf[BUFSIZE * 2];
	char *text = NULL;
	fd_set fd;
	int rc,/* sockfd = -1,*/ i = 0;

	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;

	rc = getaddrinfo(server, "whois", &hints, &res);

	if (rc) {
		sprintf(buf, "%s: getaddrinfo: %s (%d)\n", server, gai_strerror(rc), rc);
		text = strdup(buf);
	} else {
		/* print all returned addresses just debuging purposes */
		for (aip = res ; aip != NULL ; aip = aip->ai_next) {
			rc = getnameinfo(aip->ai_addr, aip->ai_addrlen,
				hbuf, sizeof(hbuf), sbuf, sizeof(sbuf),
				NI_NUMERICHOST);
			if (rc) {
				printf("[%d] getnameinfo: %s (%d)\n",
					i, gai_strerror(rc), rc);
			} else {
				/*
				printf("[%d] hostname: %s numeric addr: %s port: %s\n",
					i, server, hbuf, sbuf);
				*/
			}

			sockfd = socket(aip->ai_family, aip->ai_socktype, aip->ai_protocol);
			if (sockfd == -1) {
				sprintf(buf, "%s: socket: %s (%d)\n", server, strerror(sockfd), sockfd);
				text = strdup(buf);
				freeaddrinfo(res);
				return text;
			}

			signal(SIGALRM, gwhois_network_sigfunc);
			alarm(TIMEOUT);
			rc = connect(sockfd, aip->ai_addr, aip->ai_addrlen);
			alarm(0);

			if (errno == EBADF) {
				errno = ETIMEDOUT;
				sprintf(buf, "%s: connect: %s (%d)\n", server, strerror(rc), rc);
				text = strdup(buf);
				return text;
			}

			if (rc) {
				sprintf(buf, "%s: connect: %s (%d)\n", server, strerror(rc), rc);
				text = strdup(buf);
				close(sockfd);
			} else {
				object = realloc(object, sizeof(object) + 2);
				strcat(object, "\r\n");
				text = malloc(1);
				memset(text, 0, sizeof(text));
				write(sockfd, object, strlen(object));
				object[strlen(object) - 2] = '\0';
				object = realloc(object, sizeof(object) - 2);
				while (1) {
					FD_ZERO(&fd);
					FD_SET(sockfd, &fd);
					tv.tv_sec = TIMEOUT;
					tv.tv_usec = 0;
					rc = select(sockfd + 1, &fd, NULL, NULL, &tv);
					if (rc == 0) {
						sprintf(buf, "Read from %s timed out.", server);
						text = realloc(text, strlen(buf) + 1);
						sprintf(text, "%s", buf);
						break;
					}
					memset(buf, 0, sizeof(buf));
					rc = read(sockfd, buf, sizeof(buf)-1);
					if (rc > 0) {
						text = realloc(text, strlen(text) + strlen(buf) + 1);
						sprintf(text,"%s%s", text, buf);
					} else {
						break;
					}
				}
				close(sockfd);
				break;
			}
			++i;
		}
	}

/*	return strdup(text); */
	return text;
}
