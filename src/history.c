/* src/history.c - Routines for reading and writing history entries.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>

#include "history.h"
#include "server.h"
#include "field.h"
#include "toolbar.h"
#include "error.h"
#include "extern.h"

const gchar *hlist_item_data_key="list_item_data";

char *
gwhois_history_get_path(void)
{
	char *file;

	file = malloc(strlen(getenv("HOME")) + strlen("/.gwhois/") + strlen(GWHOIS_HISTFILE) + 1);
	sprintf(file, "%s/.gwhois/%s", getenv("HOME"), GWHOIS_HISTFILE);

	return file;
}

void
gwhois_history_insert(xmlDocPtr hdoc)
{
	xmlNodePtr cur;

	if (!hdoc)
		return;

	history_list = NULL;

	cur = hdoc->root;

	while (strcmp(cur->name, "history"))
		cur = cur->next;
	cur = cur->childs;

	while (cur) {
		if (!strcmp(cur->name, "query")) {
			char *tooltip_string, *ptr;
			cur = cur->childs;

			tooltip_string = strdup("Query was done at: ");

			while (1) {
				if (!strcmp(cur->name, "date")) {
					ptr = xmlNodeListGetString(hdoc, cur->childs, 1);

					if (!ptr)
						ptr = strdup("");

					tooltip_string = realloc(tooltip_string,
						strlen(tooltip_string) + strlen(ptr) + 2);

					sprintf(tooltip_string, "%s%s ", tooltip_string, ptr);

				} else if (!strcmp(cur->name, "time")) {

					ptr = xmlNodeListGetString(hdoc, cur->childs, 1);

					if (!ptr)
						ptr = strdup("");

					tooltip_string = realloc(tooltip_string,
						strlen(tooltip_string) + strlen(ptr) + 1);

					sprintf(tooltip_string, "%s%s", tooltip_string, ptr);

				} else if (!strcmp(cur->name, "object")) {

					ptr = xmlNodeListGetString(hdoc, cur->childs, 1);

					if (ptr) {
						history_list = g_list_append(history_list, ptr);
					}
				}

				if (!cur->next)
					break;

				cur = cur->next;
			}
			cur = cur->parent;
		}
		cur = cur->next;
	}
}

void
gwhois_history_write(char *object, char *server, GtkCombo *history_combo)
{
	struct tm *tms;
	const time_t *tip;
	time_t ti;
	char *datebuf, *month, *timebuf, *hour, *min, *sec;
	int counter = 0;

	ti = time(NULL);
	tip = &ti;
	tms = localtime(tip);

	month = malloc(4);
	memset(month, 0, 4);

	if (tms->tm_mon == 0) sprintf(month, "Jan");
	if (tms->tm_mon == 1) sprintf(month, "Feb");
	if (tms->tm_mon == 2) sprintf(month, "Mar");
	if (tms->tm_mon == 3) sprintf(month, "Apr");
	if (tms->tm_mon == 4) sprintf(month, "May");
	if (tms->tm_mon == 5) sprintf(month, "Jun");
	if (tms->tm_mon == 6) sprintf(month, "Jul");
	if (tms->tm_mon == 7) sprintf(month, "Aug");
	if (tms->tm_mon == 8) sprintf(month, "Sep");
	if (tms->tm_mon == 9) sprintf(month, "Oct");
	if (tms->tm_mon == 10) sprintf(month, "Nov");
	if (tms->tm_mon == 11) sprintf(month, "Dec");

	datebuf = malloc(12);
	memset(datebuf, 0, 12);
	sprintf(datebuf, "%d-%s-%d", tms->tm_mday, month, tms->tm_year + 1900);
	free(month);

	timebuf = malloc(9);
	memset(timebuf, 0, 9);

	hour = malloc(3);
	memset(hour, 0, 3);

	min = malloc(3);
	memset(min, 0, 3);

	sec = malloc(3);
	memset(sec, 0, 3);

	if (tms->tm_hour < 10)
		sprintf(hour, "0%d", tms->tm_hour);
	else
		sprintf(hour, "%d", tms->tm_hour);

	if (tms->tm_min < 10)
		sprintf(min, "0%d", tms->tm_min);
	else
		sprintf(min, "%d", tms->tm_min);

	if (tms->tm_sec < 10)
		sprintf(sec, "0%d", tms->tm_sec);
	else
		sprintf(sec, "%d", tms->tm_sec);

	sprintf(timebuf, "%s:%s:%s", hour, min, sec);
	free(hour);
	free(min);
	free(sec);

	if (hdoc == NULL) {
		xmlNodePtr tree, subtree;

		hdoc = xmlNewDoc("1.0");
		hdoc->root = xmlNewDocNode(hdoc, NULL, "history", NULL);

		tree = xmlNewChild(hdoc->root, NULL, "query", NULL);

		subtree = xmlNewChild(tree, NULL, "date", NULL);
		subtree->childs = xmlStringGetNodeList(hdoc, datebuf);

		subtree = xmlNewChild(tree, NULL, "time", NULL);
		subtree->childs = xmlStringGetNodeList(hdoc, timebuf);

		subtree = xmlNewChild(tree, NULL, "object", NULL);
		subtree->childs = xmlStringGetNodeList(hdoc, object);

		subtree = xmlNewChild(tree, NULL, "server", NULL);
		subtree->childs = xmlStringGetNodeList(hdoc, server);

		xmlSaveFile(gwhois_history_get_path(), hdoc);
	} else {
		xmlNodePtr tree, subtree;

		tree = hdoc->root;

		while (strcmp(tree->name, "history"))
			tree = tree->next;
		tree = tree->childs;

		while (1) {
			if (!tree) {
				tree = xmlNewChild(hdoc->root, NULL, "query", NULL);

				subtree = xmlNewChild(tree, NULL, "date", NULL);
				subtree->childs = xmlStringGetNodeList(hdoc, datebuf);

				subtree = xmlNewChild(tree, NULL, "time", NULL);
				subtree->childs = xmlStringGetNodeList(hdoc, timebuf);

				subtree = xmlNewChild(tree, NULL, "object", NULL);
				subtree->childs = xmlStringGetNodeList(hdoc, object);

				subtree = xmlNewChild(tree, NULL, "server", NULL);
				subtree->childs = xmlStringGetNodeList(hdoc, server);

				break;
			}
			tree = tree->next;
		}
		xmlSaveFile(gwhois_history_get_path(), hdoc);
	}

	free(datebuf);
	free(timebuf);

	gwhois_history_insert(hdoc);

	for (counter = 0 ; counter < max_ac ; counter++) {
		if (app_index[counter].app != NULL) {
			if (app_index[counter].state == 1) {
				gtk_combo_set_popdown_strings(
					app_index[counter].app->history_combo,
					history_list);
				gtk_entry_set_text(
					GTK_ENTRY (app_index[counter].app->history_combo->entry),
					"");
			}
		}
	}
}

void
gwhois_history_delete(GtkCombo *history_combo)
{
	int counter;

	remove(gwhois_history_get_path());

	hdoc = NULL;
	history_list = NULL;

	history_list = g_list_append(history_list, "");

	for (counter = 0; counter < max_ac ; counter++) {
		if (app_index[counter].app != NULL) {
			if (app_index[counter].state == 1) {
				gtk_combo_set_popdown_strings(
					app_index[counter].app->history_combo,
					history_list);
			}
		}
	}
}

void
gwhois_history_selection(GtkWidget *list, gpointer data)
{
	GtkCombo *history_combo = GTK_COMBO(data);
	char *object, *server = NULL;
	int pos;

	if (!hdoc)
		return;

	object = gtk_editable_get_chars(GTK_EDITABLE(history_combo->entry), 0, -1);

	if (strlen(object) > 0) {
		server = gwhois_history_get_server_by_object(object);
		free(object);

		if (!server)
			return;

		pos = gwhois_history_get_server_pos(server);
		free(server);

		if (server_list && pos >= 0)
			 gtk_list_select_item(GTK_LIST (server_list), pos);
	}
}

char *
gwhois_history_get_server_by_object(char *object)
{
	xmlNodePtr cur;
	char *ptr;
	int flag = 0;

	if (!hdoc)
		return NULL;

	cur = hdoc->root;

	while (strcmp(cur->name, "history"))
		cur = cur->next;
	cur = cur->childs;

	while (cur) {
		if (!strcmp(cur->name, "query")) {
			cur = cur->childs;

			while (1) {
				if (!strcmp(cur->name, "object")) {
					ptr = xmlNodeListGetString(hdoc, cur->childs, 1);

					if (ptr) {
						if (!strcmp(ptr, object))
							flag = 1;
					}
				} else if (!strcmp(cur->name, "server")) {
					if (flag) {
						ptr = xmlNodeListGetString(hdoc, cur->childs, 1);

						if (ptr)
							return ptr;
					}
				}

				if (cur->next == NULL)
					break;

				cur = cur->next;
			}
			cur = cur->parent;
		}
		cur = cur->next;
	}

	return NULL;
}

int
gwhois_history_get_server_pos(char *server)
{
	GnomeWhoisListServer *ptr = slist;
	int i = 0;

	while (1) {
		if (!strcmp(server, ptr->hostname))
			return i;

		if (ptr->next == NULL)
			break;
		ptr = ptr->next;
		++i;
	}

	return -1;
}

int
gwhois_history_check_object(char *object)
{
	xmlNodePtr cur;

	if (!hdoc)
		return 0;

	cur = hdoc->root;

	while (strcmp(cur->name, "history"))
		cur = cur->next;
	cur = cur->childs;

	while (cur) {
		if (!strcmp(cur->name, "query")) {
			cur = cur->childs;

			while (1) {
				if (!strcmp(cur->name, "object"))
					if (!strcmp(xmlNodeListGetString(hdoc, cur->childs, 1), object))
						return 1;

					if (cur->next == NULL)
						break;

					cur = cur->next;
			}
			cur = cur->parent;
		}
		cur = cur->next;
	}
	return 0;
}
