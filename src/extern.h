/* src/extern.h - Global variables, structures, prototypes.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 1998, 1999, 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * $Id$
 */

#ifndef _GWHOIS_EXTERN_H_
#define _GWHOIS_EXTERN_H_

#include "main.h"

#define BUFSIZE 1024
#define HISTFILE "history"
#define GLOBPATH "/usr/share/gwhois"

/* #define DEBUG(x) printf("DEBUG: %s\n", x) */
#define DEBUG(src, func, msg) printf("* %s (%s) : %s\n", src, func, msg)

/* app_index[0].app is the first window, app_index[1].app is the second and so on. */
struct _app_index {
	/* this shows in which state a application windows are, 0 = dead, 1 = active
	   just so we dont try to access a closed window. */
	int state;
	/* a pointer to the memory address of application window */
	GnomeWhoisApp *app;
} *app_index;

/* app counter, the number of active application windows */
int ac, max_ac;

extern void gwhois_statusbar (int);

extern void * gwhois_statusbar_t (void *);

extern void gwhois_app_new (void);

extern void gwhois_preferences_cb (void);

#endif /* _GWHOIS_EXTERN_H_ */
