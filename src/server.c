/* src/server.c - Functions for reading and writing whois server files.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */

static const char rcsid[] = "$Id$";

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnome.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <gnome-xml/parser.h>

#include "extern.h"
#include "server.h"

char *
gwhois_server_get_path(void)
{
	struct stat file_s;
	char *file;

	/* check if user has a private server file */
	file = malloc(strlen(getenv("HOME")) + strlen("/.gwhois/") + strlen(GWHOIS_SERVFILE) + 1);
	sprintf(file, "%s/.gwhois/%s", getenv("HOME"), GWHOIS_SERVFILE);

	if (stat(file, &file_s) == 0)
		return file;

	free(file);

	/* check if  system global server file exists */
	file = malloc(strlen(GWHOIS_DATADIR) + strlen(GWHOIS_SERVFILE) + 2);

	sprintf(file, "%s/%s", GWHOIS_DATADIR, GWHOIS_SERVFILE);

	if (stat(file, &file_s) == 0)
		return file;
	else
		return NULL;
}

GnomeWhoisListServer *
gwhois_server_read(const char *file)
{
	GnomeWhoisListServer *list = malloc(sizeof(GnomeWhoisListServer));
	GnomeWhoisListServer *ptr = list;
	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseFile(file);
	cur = doc->root;

	while (strcmp(cur->name, "servers"))
		cur = cur->next;
	cur = cur->childs;
	
	while (cur != NULL)
	{
		if (!strcmp(cur->name, "server"))
		{
			memset(list, 0, sizeof(GnomeWhoisListServer));
			cur = cur->childs;
			while (1)
			{
				if (!strcmp(cur->name, "hostname"))
					list->hostname = xmlNodeListGetString(doc, cur->childs, 1);
					if (list->hostname == NULL)
						list->hostname = strdup("");
				else if (!strcmp(cur->name, "protocol"))
					list->protocol = xmlNodeListGetString(doc, cur->childs, 1);
					if (list->protocol == NULL)
						list->protocol = strdup("");
				else if (!strcmp(cur->name, "ip"))
					list->ip = xmlNodeListGetString(doc, cur->childs, 1);
					if (list->ip == NULL)
						list->ip = strdup("");
				else if (!strcmp(cur->name, "comment"))
					list->comment = xmlNodeListGetString(doc, cur->childs, 1);
					if (list->comment == NULL)
						list->comment = strdup("");

				if (cur->next == NULL)
					break;

				cur = cur->next;
			}
			cur = cur->parent;
		}
		cur = cur->next;
		if (cur != NULL) {
			list->next = malloc(sizeof(GnomeWhoisListServer));
			list = list->next;
		}
	}

	list = ptr;

	server = strdup(list->hostname);
	/* FIXME: Add autoselect in the server list */
	
	return list;
}
