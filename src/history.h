/* src/history.h - Routines for reading and writing history entries.
 *
 * Gwhois - A whois client for GNOME.
 *
 * Copyright (C) 2000, 2001 Bjorn Lindgren
 * All rights reserved.
 *
 * Author: Bjorn Lindgren <bjorn@500mhz.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * $Id$
 */

#ifndef __GWHOIS_HISTORY_H__
#define __GWHOIS_HISTORY_H__

#include <gnome-xml/parser.h>

#define GWHOIS_HISTFILE "history.xml"

/* Variables */

xmlDocPtr hdoc;

GList *history_list;

/* Prototypes */

extern char * gwhois_history_get_path (void);

extern void gwhois_history_insert (xmlDocPtr);

extern void gwhois_history_write (char *, char *, GtkCombo *);

extern void gwhois_history_delete (GtkCombo *);

extern void gwhois_history_selection (GtkWidget *, gpointer);

extern char * gwhois_history_get_server_by_object (char *);

extern int gwhois_history_get_server_pos (char *);

extern int gwhois_history_check_object (char *);

#endif /* __GWHOIS_HISTORY_H__ */
