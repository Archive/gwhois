dnl
dnl Gwhois m4 macro definitions.
dnl
dnl $Id$
dnl

dnl AM_PATH_GWHOIS()
AC_DEFUN(AM_PATH_GWHOIS, [
PREFIX=$1
echo $1

GWHOIS_DATADIR='$(prefix)/share/gwhois'
AC_SUBST(GWHOIS_DATADIR)
INCLUDES="-DGWHOIS_DATADIR=\"$(XWHOIS_SERVERS)\""

])

dnl GWHOIS_INIT()
AC_DEFUN(GWHOIS_INIT, [

dnl GWHOIS_DATADIR='-DGWHOIS_DATADIR=\"$(datadir)/gwhois\"'
dnl AC_SUBST(GWHOIS_DATADIR)

if test "$CC" = "gcc"; then
  QA_CFLAGS="-g -Wall -Wimplicit-function-declaration -Wmain -Wunused -Wmissing-prototypes -Wmissing-declarations"
  echo "GCC compiler detected, using flags: $QA_CFLAGS"
  if test "`uname -m`" = "i686"; then
    O_CFLAGS="-O3 -mcpu=i686 -march=i686"
    echo "Enabling optimizion for i686 architecture, using flags: $O_CFLAGS"
  else
    O_FLAGS="-O2"
  fi
  CFLAGS="$O_CFLAGS $QA_CFLAGS"
fi
])
