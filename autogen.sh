#!/bin/sh
# Run this to generate all the initial makefiles, etc.
# $Id$

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gwhois"
ACLOCAL_FLAGS="-I ."

make clean >/dev/null 2>&1
make distclean >/dev/null 2>&1

. $srcdir/macros/autogen.sh
